import React, { useEffect, useState, useRef } from 'react';
import axios from 'axios';
/* eslint-disable id-blacklist, no-restricted-imports, @typescript-eslint/ban-types */
import moment from 'moment-timezone';

type PropTypes = {
  event: any;
  machineId: number;
  dropRemove: any;
  reloadLabels: any;
  getPlacementId: any;
};

export const LabelEditor = ({ event, machineId, dropRemove, reloadLabels, getPlacementId }: PropTypes) => {
  const [machineSubLabels, setMachineSubLabels] = useState<any[]>([]);
  const [activeLabels, setActiveLabels] = useState<any[]>([]);

  const placementId = event.data ? event.data.placementId : getPlacementId();
  const node = useRef() as React.MutableRefObject<HTMLInputElement>;
  const timeFromFormated = moment.unix(event.time / 1000).format('YYYY-MM-DD HH:mm:ss.SSS');
  const timeToFormated = moment.unix(event.timeEnd / 1000).format('YYYY-MM-DD HH:mm:ss.SSS');
  const authToken = localStorage.getItem('authToken');

  useEffect(() => {
    const handleClickOutside = (e: any) => {
      if (node.current.contains(e.target)) {
        // inside click
        return;
      }
      dropRemove();
    };
    document.addEventListener('mousedown', handleClickOutside);

    axios
      .get(`https://new-test-api.neuronsw.com/v1/label_subcategories/?machine=${machineId}`, {
        headers: {
          Authorization: `Token ${authToken}`,
        },
      })
      .then((res) => {
        //console.log('done getting sublabels');
        setMachineSubLabels(res.data.results);
      })
      .catch(() => {
        //console.error('Could not fetch sublabels');
      });

    if (event.data) {
      setActiveLabels(event.data.labels);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [authToken, dropRemove, event, machineId]);

  const addLabel = (e: any, desc: String, id: Number) => {
    e.preventDefault();
    axios
      .post(
        'https://new-test-api.neuronsw.com/v1/labels/?tz=Europe/Prague',
        {
          description: desc,
          end_datetime: timeToFormated,
          placements: [placementId],
          start_datetime: timeFromFormated,
          subcategory: id,
          tz: 'Europe/Prague',
        },
        {
          headers: {
            Authorization: `Token ${authToken}`,
          },
        }
      )
      .then((res) => {
        //console.log('done adding label');
        setActiveLabels([
          ...activeLabels,
          {
            subcategory: res.data.subcategory,
            id: res.data.id,
            description: res.data.description,
          },
        ]);
        reloadLabels();
      })
      .catch(() => {
        //console.error('Could not add label');
      });
  };

  const removeLabel = (e: any, id: Number) => {
    e.preventDefault();
    const deletedLabel = activeLabels.find((label: any) => label.subcategory === id);

    axios
      .delete(`https://new-test-api.neuronsw.com/v1/labels/${deletedLabel.id}/?tz=Europe%2FPrague`, {
        headers: {
          Authorization: `Token ${authToken}`,
        },
      })
      .then((res) => {
        //console.log('done deleting label');
        setActiveLabels(
          activeLabels.filter((label) => {
            return label !== deletedLabel;
          })
        );
        reloadLabels();
      })
      .catch(() => {
        //console.error('Could not delete label');
      });
  };

  const isActive = (id: number) => {
    return activeLabels.find((label: any) => label.subcategory === id) !== undefined;
  };

  return (
    <div className="graph-annotation" ref={node}>
      <div className="graph-annotation__header">
        <div className="graph-annotation__user"></div>

        <div className="graph-annotation__title">{event.data ? <span>Edit label</span> : <span>Add label</span>}</div>

        <div className="graph-annotation__time">{timeFromFormated}</div>
      </div>

      <form name="ctrl.form" className="graph-annotation__body text-center">
        <div style={{ display: 'inline-block' }}>
          <div className="gf-form">
            <span className="gf-form-label width-7">Labels</span>
            <div style={style.labelList}>
              {machineSubLabels.map((machineSubLabels) =>
                isActive(machineSubLabels.id) ? (
                  <button
                    key={machineSubLabels.id}
                    className="btn btn-danger"
                    onClick={(e) => {
                      removeLabel(e, machineSubLabels.id);
                    }}
                  >
                    {machineSubLabels.name.length > 15
                      ? `${machineSubLabels.name.slice(0, 15)}...`
                      : machineSubLabels.name}
                    : {machineSubLabels.id}
                  </button>
                ) : (
                  <button
                    key={machineSubLabels.id}
                    className="btn"
                    onClick={(e) => {
                      addLabel(e, machineSubLabels.name, machineSubLabels.id);
                    }}
                  >
                    {machineSubLabels.name.length > 15
                      ? `${machineSubLabels.name.slice(0, 15)}...`
                      : machineSubLabels.name}
                    : {machineSubLabels.id}
                  </button>
                )
              )}
            </div>
          </div>

          <div className="gf-form-button-row">
            <a
              className="btn-text"
              onClick={() => {
                dropRemove();
              }}
            >
              Cancel
            </a>
          </div>
        </div>
      </form>
    </div>
  );
};

let style = {
  labelList: { display: 'grid', gridGap: '10px', gridTemplateColumns: 'repeat(4, 1fr)' },
  item: { display: 'grid', gridGap: '10px', gridTemplateColumns: 'repeat(4, 1fr)' },
};
