import axios from 'axios';
/* eslint-disable id-blacklist, no-restricted-imports, @typescript-eslint/ban-types */
import moment from 'moment-timezone';

function convert(data: any) {
  let ret: any = {};
  const toTimestamp = (s: any) => moment(s).valueOf() / 1000;

  if (data) {
    data.forEach(({ id, subcategory, start_datetime, end_datetime, meta, description }: any) => {
      const start = toTimestamp(start_datetime.replace(/Z/, '').replace(/T/, ' '));
      const end = toTimestamp(end_datetime.replace(/Z/, '').replace(/T/, ' '));
      const key = `${start}/${end}`;
      ret[key] = ret[key] || {};
      ret[key][subcategory] = true;
      ret[key].meta = meta;
      if (ret[key]['labels']) {
        ret[key]['labels'].push({ subcategory, id, description });
      } else {
        ret[key]['labels'] = [{ subcategory, id, description }];
      }
      if (ret[key]['ids']) {
        ret[key]['ids'].push(id);
      } else {
        ret[key]['ids'] = [id];
      }
    });
  }

  ret = Object.keys(ret).map((key) => {
    const [start, end] = key.split('/').map((v: any) => v - 0);
    return {
      start,
      end,
      ids: ret[key]['ids'],
      labels: ret[key].labels,
      meta: ret[key].meta,
    };
  });
  return ret;
}

export async function getAudioRegions(
  machine: number,
  start: number,
  end: number,
  channel: number,
  timezoneOffset: string
) {
  const authToken = localStorage.getItem('authToken');
  const start_time = moment.unix(start).format('YYYY-MM-DD HH:mm:ss.SSS');
  const end_time = moment.unix(end).format('YYYY-MM-DD HH:mm:ss.SSS');
  const url = 'https://new-test-api.neuronsw.com/v1/labels/';
  try {
    const {
      data: { results },
    } = await axios.get(
      `${url}?machine=${encodeURIComponent(machine)}${
        channel ? `&placement=${encodeURIComponent(channel)}` : ``
      }&from=${encodeURIComponent(start_time).replace(/\./g, '%2E')}&to=${encodeURIComponent(end_time).replace(
        /\./g,
        '%2E'
      )}&tz=${encodeURIComponent(timezoneOffset)}&ps=500`,
      {
        headers: {
          Authorization: `Token ${authToken}`,
        },
      }
    );
    return convert(results);
  } catch {
    return;
  }
}
